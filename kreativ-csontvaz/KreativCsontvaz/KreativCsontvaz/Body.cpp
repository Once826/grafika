#include "Body.h"
#include <glm/gtx/transform2.hpp>
#include "Cylinder.h"

Body::Body() {
	this->setToBindPosition();
}

Body::~Body() {}

void Body::setToBindPosition() {
	// Chest
	rootJoint = new Joint(nullptr, "chest");
	rootJoint->addTranslation(
		glm::translate(
			glm::mat4(1.f),
			glm::tvec3<float>(0, 15.f, 0)
		)
	);

	// Hip
	Joint* hip = new Joint(rootJoint, "hip");
	Bone* chestHip = new Bone(2.f, 6.f);
	rootJoint->addChild(hip, chestHip);

	// Head
	Joint* headBottom = new Joint(rootJoint, "headBottom");
	Joint* headTop = new Joint(headBottom, "headTop");
	Bone* headBottomHeadTop = new Bone(2.f, 4.f);
	rootJoint->addChild(headBottom, nullptr);
	headBottom->addChild(headTop, headBottomHeadTop);
	headBottom->addRotation(glm::rotate(
		glm::pi<float>() / 2.f,
		glm::tvec3<float>(1, 0, 0)
	));
	headBottom->addTranslation(
		glm::translate(
			glm::mat4(1.0f),
			glm::tvec3<float>(0, 6.f, 0)
		)
	);

	// Right Shoulder
	Joint* rightShoulder = new Joint(rootJoint, "rightShoulder");
	rightShoulder->addRotation(glm::rotate(
		glm::pi<float>() / 2.f,
		glm::tvec3<float>(0, 0, 1)
	));
	rightShoulder->addTranslation(
		glm::translate(
			glm::mat4(1.0f),
			glm::tvec3<float>(0, 5.f, 0)
		)
	);
	Joint* rightArm = new Joint(rightShoulder, "rightArm");
	Bone* rightShoulderArm = new Bone(0.5f, 6.f);
	rightShoulder->addChild(rightArm, rightShoulderArm);
	rootJoint->addChild(rightShoulder, nullptr);


	Joint* leftShoulder = new Joint(rootJoint, "leftShoulder");
	leftShoulder->addRotation(glm::rotate(
		-glm::pi<float>() / 2.f,
		glm::tvec3<float>(0, 0, 1)
	));
	leftShoulder->addTranslation(
		glm::translate(
			glm::mat4(1.0f),
			glm::tvec3<float>(0, 5.f, 0)
		)
	);
	Joint* leftArm = new Joint(leftShoulder, "leftArm");
	Bone* leftShoulderArm = new Bone(0.5f, 6.f);
	leftShoulder->addChild(leftArm, leftShoulderArm);
	rootJoint->addChild(leftShoulder, nullptr);


	Joint* leftHip = new Joint(hip, "leftHip");
	Joint* rightHip = new Joint(hip, "rightHip");
	hip->addChild(leftHip, nullptr);
	hip->addChild(rightHip, nullptr);
	leftHip->addTranslation(
		glm::translate(
			glm::mat4(1.0f),
			glm::tvec3<float>(-2.f, -6.f, 0)
		)
	);
	rightHip->addTranslation(
		glm::translate(
			glm::mat4(1.0f),
			glm::tvec3<float>(2.f, -6.f, 0)
		)
	);

	Joint* leftLeg = new Joint(leftHip, "leftLeg");
	Bone* leftHipLeg = new Bone(1.f, 6.f);
	leftHip->addChild(leftLeg, leftHipLeg);

	Joint* rightLeg = new Joint(rightHip, "rightLeg");
	Bone* rightHipLeg = new Bone(1.f, 6.f);
	rightHip->addChild(rightLeg, rightHipLeg);
}

void Body::render(glm::mat4 viewProj, GLuint m_loc_mvp) {
	this->rootJoint->render(viewProj, m_loc_mvp);
}