#pragma once

#include "Cylinder.h"
#include <glm/glm.hpp>
#include "Joint.h"
#include "Bone.h"

class Body {

public:
	Body();
	~Body();
	void setToBindPosition();
	void render(glm::mat4 viewProj, GLuint m_loc_mvp);
private:
	Joint* rootJoint;
};
