#include "BodyPart.h"
#include <math.h>
#include <vector>

BodyPart::BodyPart() {}

BodyPart::BodyPart(
	float height,
	float radius,
	std::vector<glm::mat4> transformations
): bone(
	height,
	radius,
	transformations
) {}

BodyPart::~BodyPart() {}

void BodyPart::render(glm::mat4 viewProj, GLuint m_loc_mvp) {
	bone.render(viewProj, m_loc_mvp);

	for (BodyPart bodyPart : children) {
		bodyPart.render(viewProj, m_loc_mvp);
	}
}

void BodyPart::addChild(BodyPart child) {
	children.push_back(child);
}