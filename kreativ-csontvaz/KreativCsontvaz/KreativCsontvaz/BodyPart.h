#pragma once

#include <glm/glm.hpp>
#include <SDL.h>
#include "Cylinder.h"

class BodyPart {

public:
	BodyPart();
	BodyPart(
		float height,
		float radius,
		std::vector<glm::mat4> transformations
	);
	~BodyPart();

	void render(glm::mat4 viewProj, GLuint m_loc_mvp);
	void addChild(BodyPart child);
private:
	std::vector<BodyPart> children;
	Cylinder bone;
};
