#include "Bone.h"

Bone::Bone() {}

Bone::~Bone() {}

Bone::Bone(float radius, float height) {
	this->cylinder = new Cylinder(radius, height);
}

void Bone::render(
	glm::mat4 viewProj,
	GLuint m_loc_mvp,
	std::vector<glm::mat4> rotations,
	std::vector<glm::mat4> translations
) {
	this->cylinder->render(viewProj, m_loc_mvp, rotations, translations);
}