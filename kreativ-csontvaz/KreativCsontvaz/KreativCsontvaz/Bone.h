#pragma once

#include "Cylinder.h"

class Joint;

class Bone {
public:
	Bone(float radius, float height);
	Bone();
	~Bone();
	void render(
		glm::mat4 viewProj,
		GLuint m_loc_mvp,
		std::vector<glm::mat4> rotations,
		std::vector<glm::mat4> translations
	);
private:
	Cylinder* cylinder;
	Joint* startJoint;
	Joint* endJoint;
};