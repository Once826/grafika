#include "Cylinder.h"

#include <math.h>
#include <vector>

Cylinder::Cylinder() {}

Cylinder::Cylinder(float radius, float height):
vertices((N + 1)* (M + 1)),
indices(3 * 2 * N * M) {
	this->radius = radius;
	this->height = height;
	initTopAndBottomVertices();
	initVertices();
	initIndices();
	initVaoVbo();
}

Cylinder::Cylinder(
	float height,
	float radius,
	std::vector<glm::mat4> transformations
):
vertices((N + 1)* (M + 1)),
indices(3 * 2 * N * M) {
	this->transformations = transformations;
	this->height = height;
	this->radius = radius;
	initTopAndBottomVertices();
	initVertices();
	initIndices();
	initVaoVbo();
}

GLuint Cylinder::getVao() {
	return vaoID;
}

Cylinder::~Cylinder() {}

const std::vector<GLushort>& Cylinder::getIndices() const {
	return indices;
}

const std::vector<Cylinder::CylinderVertex>& Cylinder::getVertices() const {
	return vertices;
}

const std::vector<glm::mat4>& Cylinder::getTransformations() const {
	if (transformations.empty()) {
		std::vector<glm::mat4> result;
		result.push_back(glm::mat4(1.f));
		return result;
	}
	return transformations;
}

std::vector<glm::mat4>& Cylinder::addTransformation(glm::mat4 transformation) {
	transformations.push_back(transformation);
	return transformations;
}

glm::vec3 Cylinder::getUV(float u, float v) {
	u *= 2 * PI;
	v *= height;

	glm::vec3 result = glm::vec3(
		radius * sin(u),
		v,
		-radius * cos(u)
	);

	return result;
}

void Cylinder::render(
	glm::mat4 viewProj,
	GLuint m_loc_mvp,
	std::vector<glm::mat4> rotations,
	std::vector<glm::mat4> translations
) {
	glBindVertexArray(getVao());
	glm::mat4 m_matWorld = glm::mat4(1.0f);
	//std::vector<glm::mat4> transformations = getTransformations();
	glm::mat4 mvp;

	for (glm::mat4 translation : translations) {
		m_matWorld *= translation;
	}

	for (glm::mat4 rotation : rotations) {
		m_matWorld *= rotation;
	}
	mvp = viewProj * m_matWorld;

	glUniformMatrix4fv(
		m_loc_mvp,
		1,
		GL_FALSE,
		&(mvp[0][0])
	);
	glDrawElements(
		GL_TRIANGLES,
		static_cast<GLsizei>(indices.size()),
		GL_UNSIGNED_SHORT,
		nullptr
	);

	int topOffset = N * M;
	glDrawArrays(
		GL_TRIANGLE_FAN,
		topOffset,
		topVertices.size()
	);
	
	int bottomOffset = topOffset + topVertices.size();
	glDrawArrays(
		GL_TRIANGLE_FAN,
		bottomOffset + 25,
		bottomVertices.size()
	);
}

void Cylinder::initTopAndBottomVertices() {
	topVertices.push_back(
		glm::vec3(
			0.f,
			height,
			0.f
		)
	);

	bottomVertices.push_back(
		glm::vec3(
			0.f,
			0.f,
			0.f
		)
	);

	int numberOfSegments = 30;
	for (int i = 0; i <= numberOfSegments; i++) {
		float theta = (2.f * M_PI * (float)i / (float)numberOfSegments);
		topVertices.push_back(
			glm::vec3(
				radius * cos(theta),
				height,
				radius * sin(theta)
			)
		);
		bottomVertices.push_back(
			glm::vec3(
				radius * cos(theta),
				0.f,
				radius * sin(theta)
			)
		);
	}
	topVertices.push_back(
		glm::vec3(
			0.f,
			height,
			0.f
		)
	);
	bottomVertices.push_back(
		glm::vec3(
			0.f,
			0.f,
			0.f
		)
	);
}

void Cylinder::initVertices() {
	float FN = (float)(N);
	float FM = (float)(M);

	for (int j = 0; j <= M; ++j) {
		for (int i = 0; i <= N; ++i) {
			float u = i / FN;
			float v = j / FM;
			int index = i + j * (N + 1);

			vertices[index].p = getUV(u, v);
			vertices[index].c = glm::normalize(vertices[index].p);
		}
	}
}

void Cylinder::initIndices() {
	for (int j = 0; j < M; ++j)
	{
		for (int i = 0; i < N; ++i)
		{
			int index = i * 6 + j * (6 * N);
			indices[index + 0] = (i)+(j) * (N + 1);
			indices[index + 1] = (i + 1) + (j) * (N + 1);
			indices[index + 2] = (i)+(j + 1) * (N + 1);
			indices[index + 3] = (i + 1) + (j) * (N + 1);
			indices[index + 4] = (i + 1) + (j + 1) * (N + 1);
			indices[index + 5] = (i)+(j + 1) * (N + 1);
		}
	}
}

void Cylinder::initVaoVbo() {
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);

	glBufferData(
		GL_ARRAY_BUFFER,
		vertices.size() * sizeof(CylinderVertex) + topVertices.size() * sizeof(glm::vec3) + bottomVertices.size() * sizeof(glm::vec3),
		nullptr,
		GL_STATIC_DRAW
	);

	// SIDES
	int topOffset = vertices.size() * sizeof(CylinderVertex);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		0,
		topOffset,
		vertices.data()
	);

	// TOP
	glBufferSubData(
		GL_ARRAY_BUFFER,
		topOffset,
		topVertices.size() * sizeof(glm::vec3),
		topVertices.data()
	);
	
	// BOTTOM
	int bottomOffset = topOffset + topVertices.size() * sizeof(glm::vec3);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		bottomOffset,
		bottomVertices.size() * sizeof(glm::vec3),
		bottomVertices.data()
	);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(CylinderVertex),
		0
	);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(CylinderVertex),
		reinterpret_cast<void*>(sizeof(glm::vec3))
	);


	glEnableVertexAttribArray(2);
	glVertexAttribPointer(
		2,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(glm::vec3),
		reinterpret_cast<void*>(topOffset)
	);


	glEnableVertexAttribArray(3);
	glVertexAttribPointer(
		3,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(glm::vec3),
		reinterpret_cast<void*>(bottomOffset)
	);

	glGenBuffers(1, &ibID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibID);
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		indices.size() * sizeof(GLushort),
		indices.data(),
		GL_STATIC_DRAW
	);
}

