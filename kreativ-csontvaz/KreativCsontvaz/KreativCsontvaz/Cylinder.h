#pragma once

#include <glm/glm.hpp>
#include <SDL.h>
#include "GLUtils.hpp"

class Cylinder {

public:
	Cylinder();
	Cylinder(
		float height,
		float radius,
		std::vector<glm::mat4> transformations
	);
	Cylinder(
		float radius,
		float height
	);
	~Cylinder();

	void render(
		glm::mat4 viewProj,
		GLuint m_loc_mvp,
		std::vector<glm::mat4> rotations,
		std::vector<glm::mat4> translations
	);

	struct CylinderVertex {
		glm::vec3 p;
		glm::vec3 c;
	};

	GLuint getVao();

	glm::vec3 getUV(float u, float v);
	const std::vector<CylinderVertex>& getVertices() const;
	const std::vector<GLushort>& getIndices() const;
	const std::vector<glm::mat4>& getTransformations() const;
	std::vector<glm::mat4>& addTransformation(glm::mat4);

private:
	float height = 0;
	float radius = 0;
	const float PI = (float)(M_PI);
	static const int N = 20;
	static const int M = 20;
	void initVertices();
	void initIndices();

	void initTopAndBottomVertices();
	
	void initVaoVbo();

	std::vector<CylinderVertex> vertices;
	std::vector<GLushort> indices;
	std::vector<glm::mat4> transformations;

	std::vector<glm::vec3> topVertices;
	std::vector<glm::vec3> bottomVertices;
	std::vector<GLushort> topIndices;

	GLuint vaoID = 0;
	GLuint vboID = 0;
	GLuint ibID = 0;
};
