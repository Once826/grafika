#include "Joint.h"
#include "Bone.h"

Joint::Joint(Joint* parent, std::string name) {
    this->parent = parent;
    this->name = name;
}

Joint::~Joint() {}

void Joint::addChild(Joint* joint, Bone* bone) {
    children[joint] = bone;
    glm::vec3 childPosition = position;
    for (const glm::mat4& translation : translations) {
        childPosition += glm::vec3(translation[3].x, translation[3].y, translation[3].z);
    }
    joint->position = childPosition;
}

void Joint::render(glm::mat4 viewProj, GLuint m_loc_mvp) {
    for (auto& item : children) {
        Joint* joint = item.first;
        Bone* bone = item.second;

        joint->render(viewProj, m_loc_mvp, this->rotations, this->translations);
        if (bone != nullptr) {
            bone->render(viewProj, m_loc_mvp, this->rotations, this->translations);
        }
    }
}

void Joint::render(
    glm::mat4 viewProj,
    GLuint m_loc_mvp,
    std::vector<glm::mat4> rotations,
    std::vector<glm::mat4> translations
) {
    for (glm::mat4 rotation : this->rotations) {
        rotations.push_back(rotation);
    }

    for (glm::mat4 translation : this->translations) {
        translations.push_back(translation);
    }
    
    for (auto& item: children) {
        Joint* joint = item.first;
        Bone* bone = item.second;

        joint->render(viewProj, m_loc_mvp, rotations, translations);
        if (bone != nullptr) {
            bone->render(viewProj, m_loc_mvp, rotations, translations);
        }
    }
}

void Joint::addRotation(glm::mat4 rotation) {
    this->rotations.push_back(rotation);
}

void Joint::addTranslation(glm::mat4 translation) {
    this->translations.push_back(translation);
}


