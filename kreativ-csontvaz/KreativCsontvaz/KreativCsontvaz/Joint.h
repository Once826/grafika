#pragma once

#include <map>
#include <vector>
#include <glm/glm.hpp>
#include <SDL.h>
#include "GLUtils.hpp"
#include "Bone.h"

class Bone;

class Joint {
public:
	Joint(Joint* parent, std::string name);
	~Joint();
	void addChild(Joint* joint, Bone* bone);
	void render(glm::mat4 viewProj, GLuint m_loc_mvp);
	void render(
		glm::mat4 viewProj,
		GLuint m_loc_mvp,
		std::vector<glm::mat4> rotations,
		std::vector<glm::mat4> translations
	);
	void addRotation(glm::mat4 rotation);
	void addTranslation(glm::mat4 translation);
private:
	Joint* parent;
	glm::vec3 position;

	std::map<Joint*, Bone*> children;
	std::vector<glm::mat4> rotations;
	std::vector<glm::mat4> translations;
	std::string name;
};