#include "MyApp.h"

#include <math.h>
#include <vector>
#include "Body.h"


CMyApp::CMyApp() {
	camera.SetView(
		glm::vec3(6 * sin(1), 6, 6 * cos(1)),
		glm::vec3(0, 0, 0),
		glm::vec3(0, 1, 0)
	);
	body = new Body();
}

CMyApp::~CMyApp() {}

bool CMyApp::Init() {
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	// SHADERS
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER,		"myVert.vert");
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER,	"myFrag.frag");
	m_programID = glCreateProgram();
	glAttachShader(m_programID, vs_ID);
	glAttachShader(m_programID, fs_ID);
	glBindAttribLocation(
		m_programID,
		0,				
		"vs_in_pos"
	);
	glBindAttribLocation( m_programID, 1, "vs_in_col");
	glLinkProgram(m_programID);
	GLint infoLogLength = 0, result = 0;
	glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
	glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (GL_FALSE == result || infoLogLength != 0)
	{
		std::vector<char> VertexShaderErrorMessage(infoLogLength);
		glGetProgramInfoLog(m_programID, infoLogLength, nullptr, VertexShaderErrorMessage.data());
		std::cerr << "[glLinkProgram] Shader linking error:\n" << &VertexShaderErrorMessage[0] << std::endl;
	}
	glDeleteShader( vs_ID );
	glDeleteShader( fs_ID );

	m_loc_mvp = glGetUniformLocation( m_programID, "MVP");
	camera.SetProj(
		glm::radians(60.0f),
		640.0f / 480.0f,
		0.01f,
		1000.0f
	);
	return true;
}

void CMyApp::Update() {
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	camera.Update(delta_time);

	last_time = SDL_GetTicks();
}

void CMyApp::Render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(m_programID);
	glm::mat4 viewProj = camera.GetViewProj();

	body->render(viewProj, m_loc_mvp);
	glUseProgram(0);
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key) {
	camera.KeyboardDown(key);
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key) {
	camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse) {
	camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse) {}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse) {}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel) {}

// a két paraméterben az új ablakméret szélessége (_w) és magassága (_h) található
void CMyApp::Resize(int _w, int _h) {
	glViewport(0, 0, _w, _h);
	camera.Resize(_w, _h);
}

void CMyApp::Clean() {
	glDeleteBuffers(1, &m_vboID);
	glDeleteBuffers(1, &m_ibID);
	glDeleteVertexArrays(1, &m_vaoID);

	glDeleteProgram(m_programID);
}
